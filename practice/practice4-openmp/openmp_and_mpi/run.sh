#! /usr/bin/env bash
#
#SBATCH --ntasks=8
#SBATCH --cpus-per-task=2
#SBATCH --partition=RT
#SBATCH --job-name=mpi_with_openmp
#SBATCH --comment="Run mpi from config"
#SBATCH --output=out.txt
#SBATCH --error=error.txt
mpiexec ./a.out
