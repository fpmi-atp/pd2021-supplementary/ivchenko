#include <iostream>
#include <omp.h>
#include <unistd.h>

int main(int argc, char **argv) {
    int sum = 0;
    
    omp_set_num_threads(8);
        
    # pragma omp parallel 
    {
        int thread_num = omp_get_thread_num();
        int cur_sum = 0;
        
        //# pragma omp nowait
        # pragma omp for collapse(2)
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 20000; j++) {
                cur_sum += 1;
            }
        }
        # pragma omp critical
        {
            std::cout << "Thread " << thread_num << " got sum: " << cur_sum << std::endl;
        }
        #pragma omp atomic
        sum += cur_sum;
    }
    
    std::cout << "Result sum: "  << sum << std::endl;
}